# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2022/06/22 19:19:03 by davifah           #+#    #+#              #
#    Updated: 2022/11/01 09:44:35 by dfarhi           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

FILES		= $(wildcard *.cpp)

OBJS		= ${FILES:.cpp=.o}

NAME		= fixed-type

CC			= c++
CC_OPTIONS	= -Wall -Wextra -Werror

INCLUDES	=
LIB			=

${NAME}:	${OBJS}
			${CC} ${CC_OPTIONS} ${INCLUDES} -o ${NAME} ${OBJS} ${LIB}

SYSTEM		= $(shell uname -s)

STD98		= 0

ifeq ($(STD98), 1)
  CC_OPTIONS := $(CC_OPTIONS) -std=c++98 -pedantic
  ifeq ($(SYSTEM), Linux)
  endif
  ifeq ($(SYSTEM), Darwin)
  CC := gcc-12
  endif
endif

.cpp.o:
			${CC} ${CC_OPTIONS} -c ${INCLUDES} $< -o ${<:.cpp=.o}

all:		${NAME}

AddressSanitizer:	CC_OPTIONS := ${CC_OPTIONS} -fsanitize=address -g
ifeq ($(SYSTEM), Linux)
AddressSanitizer:	CC_OPTIONS := ${CC_OPTIONS} -static-libasan
endif
AddressSanitizer:	${NAME}

clean:
			rm -f ${OBJS}

fclean:		clean
			rm -f ${NAME}

re:			fclean all

.PHONY:		all clean fclean re AddressSanitizer
