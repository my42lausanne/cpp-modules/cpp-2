/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: davifah <dfarhi@student.42lausanne.ch      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/07/05 16:29:30 by davifah           #+#    #+#             */
/*   Updated: 2022/10/31 15:02:38 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Fixed.hpp"
#include <iostream>
#include <cmath>

using std::cout;
using std::endl;

Fixed::Fixed( void )
{
	cout << "Default constructor called\n";
	this->value = 0;
}

Fixed::Fixed( const int _value )
{
	cout << "Int constructor called\n";
	this->value = _value * 1 << fract_bits;
}

//Roundf function is used to add more precision to the fixed-type class
Fixed::Fixed( const float _value )
{
	cout << "Float constructor called\n";
	this->value = int(roundf(_value * float(1 << fract_bits)));
}

Fixed::Fixed( const Fixed &f )
{
	cout << "Copy constructor called\n";
	*this = f;
}

Fixed& Fixed::operator = ( const Fixed &f )
{
	cout << "Copy assignment operator called\n";
	this->value = f.getRawBits();
	return (*this);
}

std::ostream& operator << ( std::ostream& output, const Fixed& f )
{
	output << f.toFloat();
	return (output);
}

Fixed::~Fixed( void )
{
	cout << "Destructor called\n";
}

int	Fixed::getRawBits( void ) const
{
	return (this->value);
}

void	Fixed::setRawBits( const int raw )
{
	this->value = raw;
}

int	Fixed::toInt( void ) const
{
	return (int(this->value / int(1 << fract_bits)));
}

float	Fixed::toFloat( void ) const
{
	return (float(this->value / float(1 << fract_bits)));
}
