/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: davifah <dfarhi@student.42lausanne.ch      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/07/05 16:14:07 by davifah           #+#    #+#             */
/*   Updated: 2022/08/05 21:05:21 by davifah          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FIXED_HPP
# define FIXED_HPP

#include <iostream>

class Fixed
{
	private:
		int					value;
		static const int	fract_bits = 8;

	public:
		Fixed( void );
		Fixed( const int _value );
		Fixed( const float _value );
		Fixed( const Fixed& f );

		Fixed& operator = ( const Fixed& f );
		friend std::ostream& operator << ( std::ostream& output, const Fixed& f );

		~Fixed( void );

		int		getRawBits( void ) const;
		void	setRawBits( int const raw );
		int		toInt( void ) const;
		float	toFloat( void ) const;
};

#endif
