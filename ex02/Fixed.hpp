/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: davifah <dfarhi@student.42lausanne.ch      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/07/05 16:14:07 by davifah           #+#    #+#             */
/*   Updated: 2022/10/31 15:41:08 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FIXED_HPP
# define FIXED_HPP

#include <iostream>

class Fixed
{
	private:
		int					value;
		static const int	fract_bits = 8;

	public:
		Fixed( void );
		Fixed( const int _value );
		Fixed( const float _value );
		Fixed( const Fixed& f );

		Fixed& operator = ( const Fixed& f );
		friend std::ostream& operator << ( std::ostream& output, const Fixed& f );
		bool operator > ( const Fixed& f ) const;
		bool operator < ( const Fixed& f ) const;
		bool operator >= ( const Fixed& f ) const;
		bool operator <= ( const Fixed& f ) const;
		bool operator == ( const Fixed& f ) const;
		bool operator != ( const Fixed& f ) const;
		Fixed operator + ( const Fixed& f ) const;
		Fixed operator - ( const Fixed& f ) const;
		Fixed operator + ( void ) const;
		Fixed operator - ( void ) const;
		Fixed operator * ( const Fixed& f ) const;
		Fixed operator / ( const Fixed& f ) const;
		Fixed& operator ++ ( void );
		Fixed operator ++ ( int );
		Fixed& operator -- ( void );
		Fixed operator -- ( int );

		static Fixed& min( Fixed& f1, Fixed& f2);
		static const Fixed& min( const Fixed& f1, const Fixed& f2);
		static Fixed& max( Fixed& f1, Fixed& f2);
		static const Fixed& max( const Fixed& f1, const Fixed& f2);

		~Fixed( void );

		int		getRawBits( void ) const;
		void	setRawBits( int const raw );
		int		toInt( void ) const;
		float	toFloat( void ) const;
};

#endif
