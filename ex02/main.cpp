/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: davifah <dfarhi@student.42lausanne.ch      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/07/05 16:13:25 by davifah           #+#    #+#             */
/*   Updated: 2022/10/31 15:45:35 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Fixed.hpp"
#include <iostream>

int main( void ) {
	Fixed a;
	Fixed const b( Fixed( 5.05f ) * Fixed( 2 ) );

	std::cout << a << std::endl;
	std::cout << ++a << std::endl;
	std::cout << a << std::endl;
	std::cout << a++ << std::endl;
	std::cout << a << std::endl;

	std::cout  << b << std::endl;

	std::cout << Fixed::max( a, b ) << std::endl;

	std::cout << "\nTesting decrements\n";
	std::cout << a << std::endl;
	std::cout << --a << std::endl;
	std::cout << a << std::endl;
	std::cout << a-- << std::endl;
	std::cout << a << std::endl;

	std::cout << "a = b/2 = " << (a = b / Fixed(2)) << std::endl;

	std::cout << "a > b: " << (a > b);
	std::cout << "\na < b: " << (a < b);
	std::cout << "\na >= b: " << (a >= b);
	std::cout << "\na <= b: " << (a <= b);
	std::cout << "\na == b: " << (a == b);
	std::cout << "\na != b: " << (a != b) << std::endl;

	std::cout << "a + 1 = " << (a + Fixed(1)) << std::endl;
	std::cout << "a - 1 = " << (a - Fixed(1)) << std::endl;
	std::cout << "+a = " << +a << std::endl;
	std::cout << "-a = " << -a << std::endl;

	std::cout << Fixed::min( a, b ) << std::endl;

	return 0;
}
