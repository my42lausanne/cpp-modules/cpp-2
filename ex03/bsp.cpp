/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bsp.cpp                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/08/06 11:23:39 by dfarhi            #+#    #+#             */
/*   Updated: 2022/10/31 16:01:44 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Fixed.hpp"
#include "Point.hpp"
#include <iostream>

bool	bsp( Point const a, Point const b, Point const c, Point const point)
{
	if (Point::pointIsInLineSegment(a, b, point)
		|| Point::pointIsInLineSegment(b, c, point)
		|| Point::pointIsInLineSegment(c, a, point))
		return false;
	if (Point::calculateTriangleArea(point, a, b)
			+ Point::calculateTriangleArea(point, b, c)
			+ Point::calculateTriangleArea(point, c, a)
			== Point::calculateTriangleArea(a, b, c))
		return true;
	return false;
}
