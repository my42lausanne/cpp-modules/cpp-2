/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: davifah <dfarhi@student.42lausanne.ch      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/07/05 16:13:25 by davifah           #+#    #+#             */
/*   Updated: 2022/10/31 15:21:36 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Fixed.hpp"
#include "Point.hpp"
#include <iostream>

bool	bsp( Point const a, Point const b, Point const c, Point const point);

int main( void ) {
	Point a;
	Point b( 10, 30 );
	Point c( 20, 0 );
	Point inside( 1, 1 );
	Point on_edge( 0, 0 );
	Point outside( 100, 0 );

	std::cout << "inside: " << bsp(a, b, c, inside) << std::endl;
	std::cout << "on edge: " << bsp(a, b, c, on_edge) << std::endl;
	std::cout << "outside: " << bsp(a, b, c, outside) << std::endl;
	return (0);
}
