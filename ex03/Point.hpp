/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Point.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/08/06 11:01:25 by dfarhi            #+#    #+#             */
/*   Updated: 2022/08/08 14:29:41 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef POINT_HPP
# define POINT_HPP

#include "Fixed.hpp"
#include <iostream>

class Point
{
	private:
		Fixed const x;
		Fixed const y;

	public:
		Point( void );
		Point( const float _x, const float _y );
		Point( const Point& p );
		Point& operator = (const Point& p );

		~Point( void );

		static Fixed calculateTriangleArea( const Point a, const Point b, const Point c );
		static Fixed sign( const Point a, const Point b, const Point c );
		static bool pointIsInLineSegment( const Point a, const Point b, const Point p );
};

#endif
