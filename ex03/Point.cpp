/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Point.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/08/06 11:01:07 by dfarhi            #+#    #+#             */
/*   Updated: 2022/08/08 14:37:07 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Point.hpp"
#include "Fixed.hpp"

Point::Point( void ) : x(Fixed( 0 )), y(Fixed( 0 ))
{
}

Point::Point( const float _x, const float _y ) : x(Fixed( _x )), y(Fixed( _y ))
{
}

Point::Point( const Point& p ) : x(p.x), y(p.y)
{
}

Point& Point::operator = (const Point& p )
{
	(void)p;
	return (*this);
}

Point::~Point( void )
{
}

bool Point::pointIsInLineSegment( const Point a, const Point b, const Point p )
{

	return ((Fixed::min(a.x, b.x) <= p.x && p.x <= Fixed::max(a.x, b.x))
		&& (Fixed::min(a.y, b.y) <= p.y && p.y <= Fixed::max(a.y, b.y))
		&& (p.y - a.y) == (b.y - a.y) / (b.x - a.x) * (p.x - a.x));
}

Fixed Point::calculateTriangleArea( const Point a, const Point b, const Point c )
{
	Fixed A = (a.x * (b.y - c.y) + b.x * (c.y - a.y) + c.x * (a.y - b.y)) / 2;
	return (A < 0 ? A * - 1 : A);
}

Fixed Point::sign( const Point a, const Point b, const Point c )
{
	return ((a.x - c.x) * (b.y - c.y) - (b.x - c.x) * (a.y - c.y));
}
