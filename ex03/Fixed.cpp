/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: davifah <dfarhi@student.42lausanne.ch      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/07/05 16:29:30 by davifah           #+#    #+#             */
/*   Updated: 2022/10/31 15:42:26 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Fixed.hpp"
#include <iostream>
#include <cmath>

#define LOG_MSG 0

using std::cout;
using std::endl;

Fixed::Fixed( void )
{
	if (LOG_MSG)
		cout << "Default constructor called\n";
	this->value = 0;
}

Fixed::Fixed( const int _value )
{
	if (LOG_MSG)
		cout << "Int constructor called\n";
	this->value = _value << fract_bits;
}

Fixed::Fixed( const float _value )
{
	if (LOG_MSG)
		cout << "Float constructor called\n";
	this->value = int(roundf(_value * float(1 << fract_bits)));
}

Fixed::Fixed( const Fixed &f )
{
	if (LOG_MSG)
		cout << "Copy constructor called\n";
	*this = f;
}

Fixed& Fixed::operator = ( const Fixed &f )
{
	if (LOG_MSG)
		cout << "Copy assignment operator called\n";
	this->value = f.getRawBits();
	return (*this);
}

std::ostream& operator << ( std::ostream& output, const Fixed& f )
{
	output << f.toFloat();
	return (output);
}

Fixed::~Fixed( void )
{
	if (LOG_MSG)
		cout << "Destructor called\n";
}

int	Fixed::getRawBits( void ) const
{
	return (this->value);
}

void	Fixed::setRawBits( const int raw )
{
	this->value = raw;
}

int	Fixed::toInt( void ) const
{
	return (int(this->value / int(1 << fract_bits)));
}

float	Fixed::toFloat( void ) const
{
	return (float(this->value / float(1 << fract_bits)));
}

bool Fixed::operator > ( const Fixed& f ) const
{
	return (this->value > f.value);
}

bool Fixed::operator < ( const Fixed& f ) const
{
	return (this->value < f.value);
}

bool Fixed::operator >= ( const Fixed& f ) const
{
	return (this->value >= f.value);
}

bool Fixed::operator <= ( const Fixed& f ) const
{
	return (this->value <= f.value);
}

bool Fixed::operator == ( const Fixed& f ) const
{
	return (this->value == f.value);
}

bool Fixed::operator != ( const Fixed& f ) const
{
	return (this->value != f.value);
}

Fixed Fixed::operator + ( const Fixed& f ) const
{
	Fixed	tmp;
	tmp.setRawBits( this->getRawBits() + f.getRawBits() );
	return (tmp);
}

Fixed Fixed::operator - ( const Fixed& f ) const
{
	Fixed	tmp;
	tmp.setRawBits( this->getRawBits() - f.getRawBits() );
	return (tmp);
}

Fixed Fixed::operator + ( void ) const
{
	return (*this);
}

Fixed Fixed::operator - ( void ) const
{
	Fixed	tmp;
	tmp.setRawBits( - this->getRawBits() );
	return (tmp);
}

Fixed Fixed::operator * ( const Fixed& f ) const
{
	Fixed	tmp;
	tmp.setRawBits( (long(this->getRawBits()) * long(f.getRawBits())) >> this->fract_bits );
	return (tmp);
}

Fixed Fixed::operator / ( const Fixed& f ) const
{
	Fixed	tmp;
	tmp.setRawBits( (long(this->getRawBits()) << this->fract_bits) / long(f.getRawBits()) );
	return (tmp);
}

//++fixed
Fixed& Fixed::operator ++ ( void )
{
	this->setRawBits(this->getRawBits() + 1);
	return (*this);
}

//fixed++
Fixed Fixed::operator ++ ( int)
{
	Fixed	tmp( *this );
	this->setRawBits(this->getRawBits() + 1);
	return (tmp);
}

//--fixed
Fixed& Fixed::operator -- ( void )
{
	this->setRawBits(this->getRawBits() - 1);
	return (*this);
}

//fixed--
Fixed Fixed::operator -- ( int )
{
	Fixed	tmp( *this );
	this->setRawBits(this->getRawBits() - 1);
	return (tmp);
}

Fixed& Fixed::min( Fixed& f1, Fixed& f2)
{
	return (f1 <= f2 ? f1 : f2);
}

const Fixed& Fixed::min( const Fixed& f1, const Fixed& f2)
{
	return (f1.getRawBits() <= f2.getRawBits() ? f1 : f2);
}

Fixed& Fixed::max( Fixed& f1, Fixed& f2)
{
	return (f1 >= f2 ? f1 : f2);
}
const Fixed& Fixed::max( const Fixed& f1, const Fixed& f2)
{
	return (f1.getRawBits() >= f2.getRawBits() ? f1 : f2);
}
