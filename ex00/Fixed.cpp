/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: davifah <dfarhi@student.42lausanne.ch      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/07/05 16:29:30 by davifah           #+#    #+#             */
/*   Updated: 2022/07/05 16:58:00 by davifah          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Fixed.hpp"
#include <iostream>

using std::cout;
using std::endl;

Fixed::Fixed( void )
{
	cout << "Default constructor called\n";
	this->value = 0;
}

Fixed::Fixed( const Fixed &f )
{
	cout << "Copy constructor called\n";
	*this = f;
}

Fixed& Fixed::operator = ( const Fixed &f )
{
	cout << "Copy assignment operator called\n";
	this->value = f.getRawBits();
	return (*this);
}

Fixed::~Fixed( void )
{
	cout << "Destructor called\n";
}

int	Fixed::getRawBits( void ) const
{
	cout << "getRawBits member function called\n";
	return (this->value);
}

void	Fixed::setRawBits( const int raw )
{
	cout << "setRawBits member function called\n";
	this->value = raw;
}
